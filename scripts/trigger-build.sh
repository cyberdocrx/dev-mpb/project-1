#! /bin/sh

showHelp() {
# `cat << EOF` This means that cat should stop reading when EOF is detected
cat << EOF  
Usage: trigger-build.sh 
Trigger a downstream project build

-h, -help,          --help                  Display help

-t, --trggered-project-name     NAME
-p, --project-name              NAME
-i, --trggered-by-id            ID
-n, --trggered-branch-name      NAME
-j, --job                       NAME
-c, --ci-project-namespace      NAME
-r, --ci-build-ref              REF
-o, --output                    FILENAME
-u, --upstream-project          NAME
EOF
# EOF is found above and hence cat command stops reading. This is equivalent to echo but much neater when printing out.
}

export triggered_project_name=""
export project_name=""
export triggered_by_id=""
export triggered_branch_name=""
export job=""
export ci_project_namespace=""
export ci_build_ref=""
export downstream_project=""


# $@ is all command line parameters passed to the script.
# -o is for short options like -v
# -l is for long options with double dash like --version
# the comma separates different long options
# -a is for long options with single dash like -version
options=$(getopt -l "help,triggered-by:,triggered-project-name:,project-name:,triggered-by-id:,triggered-branch-name:,job:,ci-project-namespace:,ci-build-ref:,output:,downstream-project:,triggered-job-id:,triggered-pipeline-id:" -o "ht:p:i:n:j:c:r:o:d:" -a -- "$@")

# set --:
# If no arguments follow this option, then the positional parameters are unset. Otherwise, the positional parameters 
# are set to the arguments, even if some of them begin with a ‘-’.
eval set -- "$options"

while true
do
case $1 in
-h|--help) 
    showHelp
    exit 0
    ;;
--triggered-by) 
    shift
    export triggered_by=$1
    ;;
-t|--triggered-project-name) 
    shift
    export triggered_project_name=$1
    ;;
-p|--project-name)
    shift
    export project_name=$1
    ;;
-i|--triggered-by-id) 
    shift
    export triggered_by_id=$1
    ;;
--triggered-job-id) 
    shift
    export triggered_job_id=$1
    ;;
--triggered-pipeline-id) 
    shift
    export triggered_pipeline_id=$1
    ;;
-n|--triggered-branch-name) 
    shift
    export triggered_branch_name=$1
    ;;
-j|--job)
    shift
    export job=$1
    ;;
-c|--ci-project-namespace) 
    shift
    export ci_project_namespace=$1
    ;;
-r|--ci-build-ref) 
    shift
    export ci_build_ref=$1
    ;;
-d|--downstream-project) 
    shift
    export downstream_project=$1
    ;;
--)
    shift
    break;;
esac
shift
done

if [ -z "$project_name" ]
then
    showHelp
    exit 1
fi

# urlencode namespace to replace / with %2F
downstream_project_namespace=`echo ${ci_project_namespace} | sed 's@/@%2F@g'`


curl  -sS -X POST -F token=$CI_JOB_TOKEN -F ref=${ci_build_ref} -F variables[TRIGGERED_BRANCH_NAME]=${triggered_branch_name} -F variables[TRIGGERED_BY]=${triggered_by} -F variables[TRIGGERED_BY_ID]=${triggered_by_id} -F variables[TRIGGERED_JOB_ID]=${triggered_job_id} -F variables[TRIGGERER_PIPELINE_ID]=${triggered_pipeline_id} -F variables[TRIGGERED_PROJECT_NAME]=${triggered_project_name} https://gitlab.com/api/v4/projects/${downstream_project_namespace}%2F${downstream_project}/trigger/pipeline 
